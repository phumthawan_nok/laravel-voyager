#!/bin/bash
set -e

if [ -z "$1" ]; then
  sed -i "s/\$SERVER_NAME/$SERVER_NAME/g" /etc/nginx/sites-enabled/app.conf
  sed -i "s/\$HTTP_X_FORWARDED_HOST/$HTTP_X_FORWARDED_HOST/g" /etc/nginx/sites-enabled/app.conf
  sed -i "s/\$REDIRECT_URL/$REDIRECT_URL/g" /etc/nginx/sites-enabled/app.conf

  cd /var/www/html && cp .env.${APP_ENV} .env \
      && cp -R docker/oauth/${APP_ENV}/* ./storage \
      && chown -R www-data:www-data ./storage \
      && rm -rf docker

  # Setup & Mount NFS
  if [ $APP_ENV == "production" ]
  then
    rpcbind && mount -a
  fi

  exec service php7.1-fpm start &
  exec nginx -g "daemon off;"
fi

exec "$@"
