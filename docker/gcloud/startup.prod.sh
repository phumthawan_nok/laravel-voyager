#! /bin/bash

# Map Host Domains
echo "35.227.243.47 oop-api.eggdigital.com" > /etc/hosts
echo "10.128.0.3 oop-api-wrapper.eggdigital.com" >> /etc/hosts

# Cloud SQL Proxy
docker pull gcr.io/cloudsql-docker/gce-proxy:1.11
docker run -d \
  -p 127.0.0.1:3306:3306 \
  gcr.io/cloudsql-docker/gce-proxy:1.11 /cloud_sql_proxy \
  -instances=online-ordering-egg:us-central1:online-ordering-egg-db-pro=tcp:0.0.0.0:3306
