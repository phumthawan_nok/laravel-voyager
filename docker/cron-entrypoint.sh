#!/bin/bash
set -e

if [ -z "$1" ]; then
  cd /var/www/html && cp .env.${APP_ENV} .env \
      && cp -R docker/oauth/${APP_ENV}/* ./storage \
      && chown -R www-data:www-data ./storage \
      && rm -rf docker

  # Set # of hard links to 1 to keep cron happy.
  touch /etc/cron.d/php /var/spool/cron/crontabs/www-data /etc/crontab

  # Setup & Mount NFS
  if [ $APP_ENV == "production" ]
  then
    rpcbind && mount -a
  fi

  exec /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
fi

exec "$@"