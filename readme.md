# Voyager\Demo

Laravel Voyager's demo



## Install

1) Start Applications
```bash
docker-compose up -d

docker exec -u root -it voyager-demo chown -R www-data:www-data /var/www/html
```

2) Run composer install
```bash
docker exec -it voyager-demo composer install

```
or
```bash
docker exec -it voyager-demo composer update

and

docker exec -it voyager-demo composer require tcg/voyager

docker exec -it voyager-demo php artisan voyager:install --with-dummy
```

3) Generate key
```bash
docker exec -it voyager-demo php artisan key:generate
```

4) migrate database



## Usage 
Demo app http://localhost:8888/admin
phpmyadmin http://localhost:8408
```bash
Username : admin@admin.com
Password : password
```


