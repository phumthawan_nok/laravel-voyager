# Pull base image
FROM registry.hub.docker.com/eggdigital/egg-docker-base:nginx-php-7.1-fpm

ENTRYPOINT ["nginx-entrypoint.sh"]

COPY composer.json artisan server.php ./
COPY app ./app/
COPY bootstrap ./bootstrap/
COPY config ./config/
COPY database ./database/
COPY resources ./resources/
COPY storage ./storage/
COPY routes ./routes/
COPY public/index.php ./public/index.php

RUN touch .env \
    && echo "APP_DEBUG=true" >> .env \
    && echo "APP_KEY=base64:uBmdmOCm017K48yQec+S5LHb5pZDDPmZ1A9JWbkWRXY=" >> .env \
    && echo "APP_URL=http://web" >> .env \
    && echo "API_URL=http://api" >> .env \
    && composer install --no-progress --profile --prefer-dist --no-interaction --no-ansi

COPY docker/php-conf/www.conf /etc/php/7.1/fpm/pool.d/www.conf
COPY docker/nginx-entrypoint.sh /usr/local/bin/
COPY docker/nginx-sites /etc/nginx/sites-enabled
COPY docker ./docker/
COPY phpunit.xml .env.testing .env.example .env.alpha .env.staging .env.production ./
COPY .git ./.git
COPY app ./app/

#RUN composer update --no-progress --profile --prefer-dist --no-interaction --no-ansi -vvv \
RUN composer dump-autoload -vvv \
    && php artisan storage:link \
    && composer clear-cache --no-ansi --quiet \
    && export SOURCE_COMMIT="${SOURCE_COMMIT:-$(git rev-parse HEAD)}" \
    && export SOURCE_TAG="${SOURCE_TAG:-$(git describe --tags --abbrev=0)}" \
    && echo -n > version.txt \
    && echo 'SOURCE_COMMIT='${SOURCE_COMMIT} >> version.txt \
    && echo 'SOURCE_TAG='${SOURCE_TAG} >> version.txt \
    && mv version.txt /var/www/html/public \
    && chown -R www-data:www-data /var/www/html \
    && rm -rf .env
